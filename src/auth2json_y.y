%{
/*!
 * \file auth2json_y.y
 *
 * \brief   Conversion fichier authent.supra en JSON.
 *
 *	Analyse syntaxique des fichier de type .supra. Assemblage des tokens et ré-écriture en JSON.
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jean-paul.ecochard@certira.cnafmail.fr>
 * \date   jeu. mai 17 10:58:27 CET 2018
 */
#ifdef TEST
#  include <errno.h>
#  include <string.h>
#endif
#include <stdio.h>
#include <gmodule.h>

#define YYERROR_VERBOSE	1

int yylex(void);
void yyerror( GString* buf, const char* error );

extern char* yytext;
%}

%union {
	char* string;
}

%token	FFIC
%token	<string> USER
%token	<string> CODE
%token	<string> PWD
%token	<string> PROFIL
%token	<string> DROIT
%token	<string> ACE
%token	EQ
%token	SEP
%token	TABOPEN
%token	TABCLOSE
%token	TABSEP
%token	<string> ID;
%type	<string> code;
%type	<string> pwd;
%type	<string> profil;
%type	<string> droit;
%type	<string> ace;
%type	<string> id;

%parse-param { GString* json_out }
%%

users:	/* empty */
	| users user
	| users FFIC { YYACCEPT; }

user: USER '{' { g_string_append( json_out, "{\"user\": {" ); } declaration '}' { g_string_append( json_out, "}}\n" ); }

declaration:	code EQ id 		{ g_string_append_printf( json_out, "\"%s\": \"%s\",", $1, $3 ); free( $3 ); }
		pwd EQ id 				{ g_string_append_printf( json_out, "\"%s\": \"%s\",", $5, $7 ); free( $7 ); }
		profil EQ id 			{ g_string_append_printf( json_out, "\"%s\": \"%s\",", $9, $11 ); free( $11 ); }
		droit EQ id 			{ g_string_append_printf( json_out, "\"%s\": \"%s\",\"Ace\":[", $13, $15); free( $15 ); }
		aces					{ g_string_append_printf( json_out, "]" ); }

aces:	aces { g_string_append( json_out, "," ); } un_ace
		| un_ace

un_ace: ace				{ g_string_append( json_out, "{" ); }
		EQ id			{ g_string_append_printf( json_out, "\"Machine\":\"%s\",", $4 ); }
		SEP				{ g_string_append( json_out, "\"clients\":" ); }
		table SEP		{ g_string_append( json_out, ",\"modeles\":" ); }
		table			{ g_string_append( json_out, "}" ); }

table:	TABOPEN {g_string_append( json_out, "[" );} id_list TABCLOSE {g_string_append( json_out, "]" );}

id_list:	id						{ g_string_append_printf( json_out, "\"%s\"", $1 ); free( $1 ); }
		| id_list TABSEP id			{ g_string_append_printf( json_out, ",\"%s\"", $3 ); free( $3 ); }

code: CODE	{ $$ = "Code"; }
pwd: PWD	{ $$ = "Pwd"; }
profil: PROFIL	{ $$ = "Profil"; }
droit: DROIT	{ $$ = "Droit"; }
ace: ACE	{ $$ = "Ace"; }
id: ID		{ $$ = strdup(yytext); }

%%

void yyerror( GString* buf, const char* error )
{
	g_string_assign( buf, error );
}

#ifdef TEST
int main( int argc, char *argv[] )
{
	GString* json_out = g_string_new( NULL );

	if( freopen( argv[1], "r", stdin ) == NULL ) {
		fprintf( stderr, "Can't reopen stdin to %s: %s\n",
			argv[1], strerror(errno) );
		return -1;
	}
	int ret = -1;
	yyparse( json_out );
	fclose( stdin );

	fprintf( stderr, "%s\n", json_out->str );

	return 0;
}
#endif
