/*
 ============================================================================
 Name		: saceauth.c
 Author	  : Ecoc
 Version	 :
 Copyright   : Tous droits réservés
 Description : Supra authentication HTTPS server in C, Ansi-style
 ============================================================================
 */
/*!
 * \file saceauth.c
 *
 * \brief	Supra authentication for ACE with HTTPS server in C.
 *
 *	Serveur HTTP/HTTPS pour authentifier les utilisateur des composants ACE (supra).
 *	Version utilisant mongoose comme serveur HTTP/HTTPS.
 *
 *	Ce serveur offre les API REST  suivantes:
 *		- authentifier
 *		- autorisation
 *		- access
 *
 * \author Ecochard Jean-Paul <jp.ecochard@free.fr>
 * \date   mer. juin 20 16:04:37 CEST 2018
 */
#ifdef HAVE_CONFIG_H
#   include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <getopt.h>
#include <sys/mman.h>

#include <gmodule.h>

#ifndef MG_ENABLE_FILESYSTEM
#    define MG_ENABLE_FILESYSTEM	1
#endif
#ifndef MG_ENABLE_SSL
#    define MG_ENABLE_SSL			1
#endif
#ifndef MG_ENABLE_THREADS
#    define MG_ENABLE_THREADS		1
#endif
#include "mongoose.h"

#include "parse_config.h"
#include "sysjrnl.h"

#ifndef MAX
#  define	   MAX(x, y) ((x) > (y) ? (x) : (y))
#endif
/*!
 * \brief Context for HTTP/HTTPS server.
 *
 * Store minimal information for HTTP/HTTP server.
 */
typedef struct ldmls_www_s {
	char*		port;			/*!< Server TCP port */
	uint32_t	ssl;			/*!< SSL on/off */
	char*		ssl_cert;		/*!< SSL signed certificate file. */
	char*		ssl_key;		/*!< SSL key file. */
} ldmls_www_t;

#ifdef HAVE_SUPRAUSERAUTHENT_H
#   include "suprauserauthent.h"
#endif

#ifndef DEF_SUFFIX_FICAUTH
# define DEF_SUFFIX_FICAUTH ".supra"
#endif
/*!
 * \brief	Verbosity level for .
 */
static int Verbose = 0;
/*!
 *	\brief Options de la ligne de commande.
 *
 *	- -V ou --verbose:			passage en mode verbeux.
 *	- -v ou --version:			numéro de version.
 *	- -c ou --config fichier:	fichier de configuration.
 */
static struct option Long_Options[] = {
	{"verbose", no_argument,		0,	'V' },
	{"version", no_argument,		0,	'v' },
	{"config", required_argument,	0,	'c' },
	{NULL,		0,					0,	0 }
};

static struct mg_serve_http_opts S_http_server_opts;

static ldmls_www_t Server;

static char* RootUser = NULL;

extern int do_access( int fd, GString* json_out, int verbose );

/* Forward ref. */
static void* start_http_server( void* arg );
/*!
 * \brief Fichier des statistiques d'accès.
 *
 * À protèger par un mutex si le serveur http est multi-thread.
 */
char StatFile[PATH_MAX];

/*!
 * \brief	Affichage d'un message d'erreur lors de la lecture du fichier de configuration.
 *
 * @param lig	Numéro de ligne de l'erreur.
 * @param msg	Message d'erreur.
 */
static void log_err( int lig, char *msg )
{
	fprintf( stderr, "Ligne %d: %s.\n", lig, msg );
}
/*!
 * \brief Read  [www] configuration section.
 *
 *	Lecture de information de la section[www] du fichier de configuration.
 *
 *	port=entier			Définition du numéro de port d'écoute du service HTTP/HTTPS.
 *	root_user=chaine	Utilisateur authentifier sans mot de passe.
 *	ssl=[on|off]		Valide l'utilisation de HTTP (off) ou HTTPS (on).
 *	ssl_cert=chaine		Chemin du fichier contenant le certificat cryptant la clé pour HTTPS.
 *	sss_key=chaine		Chemin du fichier contenant la clé X509 pour HTTPS.
 *
 * @param nb		Nombre de section [www] à lire (inutilisé).
 * @param conf		Pointeur sur la configuration analysée.
 *
 * @return	0 en cas de succès.
 */
static int creer_www( int nb, CONF* conf )
{
	char **valeurs = NULL;
	int num = 0;
	int ret;

	ret = valeurs_configuration( conf, "www", num, "port", &valeurs );
	if( ret <= 0 ) {
		fprintf( stderr,
			"Pas de paramètre \"port\" dans la section \"[www]\" n° %d.\n", num );
		return 1;
	}
	if( Verbose ) {
		fprintf( stderr, "port=%s\n", valeurs[0] );
	}
	Server.port = strdup(valeurs[0]);

	ret = valeurs_configuration( conf, "www", num, "root_user", &valeurs );
	if( ret > 0 ) {
		if( RootUser ) free( RootUser );
		RootUser = strdup( valeurs[0 ] );
		if( Verbose ) {
			fprintf( stderr, "root=%s\n", valeurs[0] );
		}
	}

	Server.ssl = 0;
	ret = valeurs_configuration( conf, "www", num, "ssl", &valeurs );
	if( ret>0 && !strcmp( valeurs[0], "on" ) ) {
		Server.ssl = 1;
		ret = valeurs_configuration( conf, "www", num, "ssl_cert", &valeurs );
		if( ret <= 0 ) {
			fprintf( stderr,
					"Pas de paramètre \"ssl_cert\" dans la section \"[www]\" n° %d.\n", num );
			return 1;
		}
		if( Verbose ) {
			fprintf( stderr, "ssl_cert=%s\n", valeurs[0] );
		}
		Server.ssl_cert = strdup(valeurs[0]);
		ret = valeurs_configuration( conf, "www", num, "ssl_key", &valeurs );
		if( ret <= 0 ) {
			fprintf( stderr,
					"Pas de paramètre \"ssl_key\" dans la section \"[www]\" n° %d.\n", num );
			return 1;
		}
		if( Verbose ) {
			fprintf( stderr, "ssl_key=%s\n", valeurs[0] );
		}
		Server.ssl_key = strdup(valeurs[0]);
	}

	return 0;
}
/*!
 * \brief Lecture du fichier de configuration.
 *
 * @param file	Chemin du fichier de configuration.
 *
 * @return 0 en cas de succès, sinon message d'erreur et errno positionné.
 */
int lire_config( char* const file )
{
	if( Verbose ) fprintf( stderr, "Enter %s( %s )\n", __func__, file );

	CONF* conf = parse_configuration( file, log_err );

	if( conf ) {
		int nb = nb_sections_configuration( conf, "www" );
		if( nb ) {
			if( creer_www( nb, conf ) ) {
				detruire_configuration( conf );
				errno = EINVAL;

				return 1;
			}
		}
	}

	detruire_configuration( conf );

	return conf != NULL ? 0 : 1;
}
/*!
 * \brief	Ouverture d'un fichier authent.
 *
 *	Cette fonction effectue l'ouverture d'un fichier d'authentification pour un utilisateur donné 'user'.
 *	'ficAuth' pointe sur une zone fournie par l'appelant de taille suffisante pour contenir MAXPATHLEN caractères.
 *
 * @param user		Nom de l'utilisateur.
 * @param ficAuth	Zone contenant le chemin complet du fichier.
 *
 * @return	-1 (errno positionné) ou un descripteur sur le fichier ouvert.
 */
static int auth_open( char* user, char* ficAuth )
{
	char* rep = NULL;

	if( Verbose ) fprintf( stderr, "Enter %s( %s )\n", __func__, user );

	if( (rep = getenv("ACE_SUPRA_AUTH")) ) {
		snprintf( ficAuth, MAXPATHLEN, "%s/%s%s", rep, user, DEF_SUFFIX_FICAUTH );
	} else {
		rep = getenv( "ACEB" );
		if( rep == NULL ) {
			rep = getenv( "SAPHIRHOME" );
		}
		if( rep == NULL ) {
			rep = "/home/ace";
		}
		snprintf( ficAuth, MAXPATHLEN, "%s/conf/%s%s", rep, user, DEF_SUFFIX_FICAUTH );
	}

	if( Verbose ) fprintf( stderr, "%s:: open( %s )\n", __func__, ficAuth );

	int fd = open( ficAuth, O_RDONLY );
	if( fd < 0 ) {
		return -1;
	}

	return fd;
}
/*!
 * \brief Authentifier l'utilisateur.
 *
 *	Analyse du dépot d'authentification centralisé pour déterminer si l'utilisateur cité
 * est bien authentifié.
 *
 * \param	user	Utilisateur à authentifier.
 * \param	pwd		Mot de passe de l'utilisateur.
 * \param	mach	Optionnel nom de l'atelier sur lequel l'utilisateur s'authentifie.
 */
static int do_authent( char *user, char *pwd, char* mach )
{
	char*	rep = NULL;
	char	ficAuth[MAXPATHLEN];

	if( Verbose ) fprintf( stderr, "Enter %s( %s, %s, %s )\n", __func__, user, pwd, mach?mach:"NULL" );

	if( RootUser && !strcmp( user, RootUser ) ) return 0;

	int fd = auth_open( user, ficAuth );
	if( fd < 0 ) {
		if( Verbose ) fprintf( stderr, "Ouverture %s: %s.\n", ficAuth, strerror(errno) );
		return ERR_FICAUTHENT;
	}

	struct stat bufStat;

	if( fstat( fd, &bufStat ) ) {
		close( fd );
		if( Verbose ) fprintf( stderr, "fstat %s: %s.\n", ficAuth, strerror(errno) );
		return ERR_FICAUTHENT;
	}
	char* donnees = mmap( NULL, bufStat.st_size, PROT_READ, MAP_PRIVATE, fd, 0 );
	if( donnees == MAP_FAILED ) {
		close( fd );
		if( Verbose ) fprintf( stderr, "mmap %s: %s.\n", ficAuth, strerror(errno) );
		return ERR_FICAUTHENT;
	}


	char* pt = NULL;
	int ret = 1;

	if( (pt = strstr( donnees, "pwd = " )) ) {
		if( !strncmp( pwd, pt+6, strlen(pwd) ) ) {
			if( Verbose ) fprintf( stderr, "pwd: %s %s: %s.\n", user, pwd, "OK" );
			ret = 0;
		}
	}

	if( mach ) {
		size_t len = strlen( mach );
		pt = strchr( pt+6, '\n' );
		ret = 1;
		while( pt ) {
			if( (pt = strstr( pt, "ace = " )) ) {
				if( strncmp( mach, pt+6, len ) == 0 && *(pt+6+len) == '@' ) {
					if( Verbose ) fprintf( stderr, "mach: %s on %s: %s.\n", user, mach, "OK" );
					ret = 0;
					break;					// mach found.
				}
			}
			if( pt ) {
				pt = strchr( pt+6, '\n' );	// next machine.
				continue;
			}
		}
	}

	munmap( donnees, bufStat.st_size );
	close(fd);

	if( Verbose ) fprintf( stderr, "%s %s %s: %d.\n", user, pwd, mach?mach:"NULL", ret );
	return ret;
}
/*!
 * \brief Dispatcher une requête GET.
 *
 * @param c		Connexion HTTP.
 * @param hm	Message à traiter.
 */
static void dispatch_get( struct mg_connection *c, struct http_message *hm )
{
	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	char	user[32];
	char	passwd[32];

	int ret = mg_get_http_var( &hm->query_string, "user", user, sizeof(user) );
	ret = mg_get_http_var( &hm->query_string, "pwd", passwd, sizeof(passwd) );

	if( !strncmp( hm->uri.p, "/authentifier", MAX(13,hm->uri.len) ) ) {
		if( do_authent( user, passwd, NULL ) != 0 ) {
			mg_http_send_error( c, 403, "Forbidden Access" );

			return;
		}
		mg_send_head( c, 200, -1, "Content-Type: text/plain;charset=UTF-8" );
		mg_printf_http_chunk( c, "Authentification OK." );
		mg_send_http_chunk( c, "", 0 );	/* Tell client we have finished. */
	} else if( mg_vcmp( &hm->uri, "/autorisation" ) == 0 ) {
		char machine[32];

		int ret = mg_get_http_var( &hm->query_string, "machine", machine, sizeof(machine) );
		if( ret == 0 ) {
			mg_http_send_error( c, 400, "Bad Request \"machine\" parameter missing" );

			return;
		}
		if( do_authent( user, passwd, machine ) != 0 ) {
			mg_http_send_error( c, 403, "Forbidden Access" );

			return;
		}
		mg_send_head( c, 200, -1, "Content-Type: text/plain;charset=UTF-8" );
		mg_printf_http_chunk( c, "Authentification OK." );
		mg_send_http_chunk( c, "", 0 );	/* Tell client we have finished. */
	} else if( mg_vcmp( &hm->uri, "/access" ) == 0 ) {
		GString* json_out = g_string_new( NULL );
		char	ficAuth[MAXPATHLEN];
		int		fd = -1;

		fd = auth_open( user, ficAuth );
		if( fd < 0 ) {
			g_string_sprintf( json_out, "Internal error open ficAuth: %s.", strerror(errno) );
			mg_http_send_error( c, 500, json_out->str );
		} else {
			if( do_access( fd, json_out, Verbose ) != 0 ) {
				mg_http_send_error( c, 500, "Internal error" );
			} else {
				mg_send_head( c, 200, -1, "Content-Type: text/plain;charset=UTF-8" );
				mg_printf_http_chunk( c, json_out->str );
				mg_send_http_chunk( c, "", 0 );	/* Tell client we have finished. */
			}
			close( fd );
		}
		g_string_free( json_out, TRUE );
	} else {
		mg_serve_http( c, (struct http_message *)hm, S_http_server_opts );
	}

}
/*!
 * \brief Dispatcher une requête POST.
 *
 * Il n'y en a aucune actuellement.
 *
 * @param c		Connexion HTTP.
 * @param hm	Message à traiter.
 */
static void dispatch_post( struct mg_connection *c, struct http_message *hm )
{
	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );
	mg_serve_http( c, (struct http_message *)hm, S_http_server_opts );
}
/*!
 * \brief	Gestionnaire des événements HTTP.
 *
 *	Gestion de quelques événementsdu protocole HTTP (connexion, requêtes, ...)
 *
 * @param c		Connexion HTTP.
 * @param ev	Evénement à traiter.
 * @param p		Donnée.
 */
static void ev_handler( struct mg_connection *c, int ev, void *p )
{
#ifdef HAVE_GETHRTIME
	hrtime_t	debut;
	hrtime_t	fin;

	debut = gethrtime();
#else
	struct timeval	debut;
	struct timeval	fin;

	gettimeofday( &debut, NULL );
#endif

	if( ev == MG_EV_HTTP_REQUEST ) {
		if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

		struct http_message *hm = (struct http_message *)p;
		if( Verbose ) {
			fprintf( stderr, "Method: %.*s.\n", hm->method.len, hm->method.p );
			fprintf( stderr, "URI: %.*s.\n", hm->uri.len, hm->uri.p );
			fprintf( stderr, "Query: %.*s.\n", hm->query_string.len, hm->query_string.p );
		}
		// We have received an HTTP request. Parsed request is contained in `hm`.
		// Send HTTP reply to the client which shows full original request.
		if( !strncmp( hm->method.p, "GET", hm->method.len ) ) {
			dispatch_get( c, hm );
		} else if( !strncmp( hm->method.p, "POST", hm->method.len ) ) {
			dispatch_post( c, hm );
		} else {
			mg_serve_http(c, (struct http_message *)p, S_http_server_opts);
		}
#ifdef HAVE_GETHRTIME
		fin= gethrtime();
#else
		gettimeofday( &fin, NULL );
#endif
		FILE* stat = fopen( StatFile, "a" );
		if( stat ) {
			char from[32];
			from[0]='\0';
			mg_sock_addr_to_str( &c->sa, from, sizeof(from), MG_SOCK_STRINGIFY_IP|MG_SOCK_STRINGIFY_PORT );
#ifdef HAVE_GETHRTIME
			long long int duree = fin - debut;
#else
			long long int duree = ((fin.tv_sec - debut.tv_sec) * 1000000 + (fin.tv_usec - debut.tv_usec)) * 1000;
#endif
			char horo[32];
			time_t tim = time(NULL);
			strftime( horo, sizeof(horo), "%x %X", localtime(&tim) );
			fprintf( stat, "%s %s: uri %.*s lapse %lld ns\n", horo, from, hm->uri.len, hm->uri.p, duree );
			fclose( stat );
		}
	}
}
/*!
 * \brief	Lancement du thread serveur HTTP.
 *
 * @param arg	Informations de configuration du serveur HTTP.
 */
static void* start_http_server( void* arg )
{
	struct mg_mgr			 mgr;
	struct mg_connection	*c = NULL;
	struct mg_bind_opts		 bind_opts;
	ldmls_www_t* 			 server = (ldmls_www_t*)arg;
	const char				*err = NULL;
	mg_mgr_init( &mgr, NULL );

	memset( &bind_opts, 0, sizeof(bind_opts) );
#ifdef MG_ENABLE_SSL
	if( server->ssl ) {
		bind_opts.ssl_cert = server->ssl_cert;
		bind_opts.ssl_key = server->ssl_key;
		bind_opts.error_string = &err;
		if( Verbose ) fprintf( stderr, "Start SSL on port %s, cert from %s, key from %s\n",
				server->port, bind_opts.ssl_cert, bind_opts.ssl_key );
	}
#endif
	c = mg_bind_opt( &mgr, server->port, ev_handler, bind_opts );
	if( c == NULL  ) {
		fprintf( stderr, "Failed to create HTTPS listener: %s.\n", err );

		return NULL;
	}
	mg_set_protocol_http_websocket( c );
	S_http_server_opts.document_root = "share/saceauth/www";  // Root directory
	S_http_server_opts.index_files = "index.html";  // Index file name
	S_http_server_opts.enable_directory_listing = "yes";

	for( ; ; ) {
		mg_mgr_poll( &mgr, 1000 );
	}
	mg_mgr_free(&mgr);

	return NULL;
}
/*
 *	Usage message.
 */
static void usage( char *n )
{
	fprintf( stderr, "Usage: %s -V -v -c config\n", n );
	fprintf( stderr, "avec:\n-V mode verbeux.\n-v indique la version.\n" );
	fprintf( stderr, "-c fichier de configuration.\n" );
}
/*
 *	Programme principal.
 */
int main( int argc, char *argv[] )
{
	char		*file	= "saceauth.conf";
	int			 option_index = 0;
	int			 c = 0;

	while( (c = getopt_long( argc, argv, "vVc:", Long_Options,  &option_index )) != -1 ) {
		switch( c ) {
			case 'v':
				fprintf( stderr, "Version: %s\n", VERSION );
				return EXIT_SUCCESS;
			case 'V':
				Verbose = 1;
				break;
			case 'c':
				file = optarg;
				break;
			default:
				fprintf( stderr, "L'option %c est inconnue.\n", c );
				usage( argv[0] );
				return EXIT_FAILURE;
		}
	}

	if( !Verbose ) {
		int ret = -1;
		if( (ret = fork()) == -1 ) {
			return -1;
		}
		if( ret ) {
			if( Verbose ) fprintf( stderr, "Demarrage de %s: pid = %d\n", argv[0], ret );
			return 0;
		}
		/*
		 *  Dupliquer stdout et stderr.
		 */
		char log[1024];
		snprintf( log, sizeof(log), "%s/jrnl/saceauth.out", getwdir() );
		if( (ret = open( log, O_WRONLY|O_CREAT,  0660 )) < 0 ) {
			sysjrnl( LOG_ERR, "%s::open::jrnl/mservid.out: %m.", argv[0] );
			return 1;
		}
		if( dup2( ret, 2 ) < 0 ) perror( "dup(stderr)" );
		if( dup2( ret, 1 ) < 0 ) perror( "dup(stdout)" );
		/*
		 * Clore les canaux encore ouverts
		 */
		close( 0 );
		for( int i = 3; i < ret; i++ ) close( i );
		close( ret );
		/*
		 * Detacher le processus du terminal.
		 */
		if( setsid() < 0 ) {
			sysjrnl( LOG_ERR, "%s::setsid(): %m.", argv[0] );
			return 1;
		}
	}
	if( lire_config( file ) ) {
		sysjrnl( LOG_ERR, "Configuration parsing error: %m." );
		return EXIT_FAILURE;
	}

	snprintf( StatFile, sizeof(StatFile), "%s/jrnl/saceauth_access", getwdir() );

	if( start_http_server( &Server ) ) {
		sysjrnl( LOG_ERR, "Erreur au lancement du serveur WEB. Serveur désactivé.\n" );
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
