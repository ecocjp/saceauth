/*
 ============================================================================
 Name		: saceauth.c
 Author	  : Ecoc
 Version	 :
 Copyright   : Tous droits réservés
 Description : Supra authentication HTTPS server in C, Ansi-style
 ============================================================================
 */
/*!
 * \file saceauth.c
 *
 * \brief	Supra authentication for ACE with HTTPS server in C.
 *
 *	Serveur HTTP/HTTPS pour authentifier les utilisateur des composants ACE (supra).
 *	Version utilisant libmicrohttpd comme serveur HTTP/HTTPS.
 *
 *	Ce serveur offre les API REST  suivantes:
 *		- authentifier
 *		- autorisation
 *		- access
 *
 * \author Ecochard Jean-Paul <jp.ecochard@free.fr>
 * \date   mer. juin 20 16:04:37 CEST 2018
 */
#ifdef HAVE_CONFIG_H
#   include "config.h"
#else
#	error "config.h is mandatory"
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <netdb.h>

#ifdef HAVE_MMAP
#  include <sys/mman.h>
#endif
#include <sys/param.h>

#include <gmodule.h>
#include <microhttpd.h>

#include "gethome.h"
#include "parse_config.h"
#include "sysjrnl.h"

#ifndef MAX
#  define	   MAX(x, y) ((x) > (y) ? (x) : (y))
#endif

/*!
 * Authorized page.
 */
#define AUTHENT_OK	"<html><head><meta charset=\"UTF-8\"><title>Authorized request</title></head><body><p>Success: authentication succeed.</br>Authentification réussie.</p></body></html>"
/*!
 * Bad request page.
 */
#define REQUEST_ERROR	"<html><head><meta charset=\"UTF-8\"><title>Bad request</title></head><body><p>Error: malformed request syntax (parameters).</br>Syntaxe de la requète erronée (paramètres).</p></body></html>"
#define REQUEST_CODE	400
/*!
 * Unauthorized page.
 */
#define AUTHENT_ERROR	"<html><head><meta charset=\"UTF-8\"><title>Forbbiden access</title></head><body><p>Error: authentication is required and has failed.</br>L'authentification est requise et a échouée.</p></body></html>"
#define AUTHENT_CODE	403
/*!
 * Invalid method page.
 */
#define METHOD_ERROR	"<html><head><meta charset=\"UTF-8\"><title>Illegal request</title></head><body><p>Error: Method not allowed.</br>Méthode non autorisée.</p></body></html>"
#define METHOD_CODE		405
/*!
 * Invalid method page.
 */
#define NOTFOUND_ERROR	"<html><head><meta charset=\"UTF-8\"><title>Page not found</title></head><body><p>Error: URL not found.</br>URL inconnue.</p></body></html>"
#define NOTFOUND_CODE	404
/*!
 * Other errors
 */
#define HTTP_ERROR		"<html><head><meta charset=\"UTF-8\"><title>Request error</title></head><body><p>%s</p></body></html>"
#define HTTP_CODE		500
/*!
 * Header for statistics.
 */
#define STAT_HEAD		"<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>SACEAUTH STATISTICS</title><link rel=\"stylesheet\" type=\"text/css\" href=\"theme.css\"></head><body>"
/*!
 * Root directory
 */
#define ROOT_DIR	"share/saceauth/www"
/*!
 * Default index file.
 */
#define INDEX		"index.html"
/*!
 * \brief Context for HTTP/HTTPS server.
 *
 * Store minimal information for HTTP/HTTP server.
 */
typedef struct ldmls_www_s {
	char*		port;			/*!< Server TCP port */
	uint32_t	ssl;			/*!< SSL on/off */
	char*		ssl_cert;		/*!< SSL signed certificate file. */
	char*		ssl_key;		/*!< SSL key file. */
} ldmls_www_t;
/*!
 * \brief Parameters storage.
 *
 *	store parameters values from request URI.
 */
struct KeyValue
{
	unsigned int	nb;				/*!< Number of {key,value} pairs. */
	struct {
		char*	key;				/*!< Key (nul terminated char). */
		char*	val;				/*!< value (NULL, or nul terminated value). */
	} *data;						/*!< Key/value pair. */
};
/*!
 * \brief statistics
 */
struct Stats_s {
	pthread_mutex_t	mp;			/*!< Mutex pour les statistiques. */
	unsigned int	nb_request;	/*!	Number of requests. */
	unsigned int	nb_success; /*! Number of success. */
	unsigned int	nb_authent;	/*! Call to authentifier. */
	unsigned int	nb_autor;	/*! Calls to autoriser. */
	unsigned int	nb_access;	/*! Calls to access. */
	unsigned int	nb_upload;	/*! Nb of GET file. */
	time_t			duration;	/*! Requests duration in ms. */
	time_t			start;		/*! Start date. */
} Stats;

#ifdef HAVE_SUPRAUSERAUTHENT_H
#   include "suprauserauthent.h"
#endif

#ifndef DEF_SUFFIX_FICAUTH
# define DEF_SUFFIX_FICAUTH ".supra"
#endif
/*!
 * \brief	Verbosity level for .
 */
static int Verbose = 0;
/*!
 *	\brief Options de la ligne de commande.
 *
 *	- -V ou --verbose:			passage en mode verbeux.
 *	- -v ou --version:			numéro de version.
 *	- -c ou --config fichier:	fichier de configuration.
 */
static struct option Long_Options[] = {
	{"verbose", no_argument,		0,	'V' },
	{"version", no_argument,		0,	'v' },
	{"config", required_argument,	0,	'c' },
	{NULL,		0,					0,	0 }
};

static ldmls_www_t Server;

static char* RootUser = NULL;

extern int do_access( int fd, GString* json_out, int verbose );
/*!
 * \brief Fichier des statistiques d'accès.
 *
 * À protèger par un mutex si le serveur http est multi-thread.
 */
char			StatFile[PATH_MAX];
/*!
 * \brief Mutex for StatsFile;
 */
pthread_mutex_t	StatsFileMtx;
/*!
 * \brief Charge un fichier en mémoire.
 *
 *	Lire un fichier et charger le contenu dans un buffer mémoire.
 *
 * @param filename	Chemin du fichier à charger.
 *
 * @return
 */
static char *load_file( const char *filename )
{
	if( Verbose ) fprintf( stderr, "Enter %s( %s )\n", __func__, filename );

	struct stat bufStat;
	if( stat( filename, &bufStat ) != 0 ) {
		if( Verbose > 1) fprintf( stderr, "stat %s: %s.\n", filename, strerror(errno) );

		return NULL;
	}

	if( bufStat.st_size <= 0 ) return NULL;

	FILE*	fp = fopen( filename, "rb" );
	if( !fp ) return NULL;

	char *buffer = malloc( bufStat.st_size + 1 );
	if( !buffer ) {
		fclose( fp );
		return NULL;
	}

	buffer[bufStat.st_size] = '\0';

	if( bufStat.st_size != (long)fread( buffer, 1, bufStat.st_size, fp ) ) {
		free( buffer );
		buffer = NULL;
	}

	fclose( fp );
	return buffer;
}

/*!
 * \brief	Affichage d'un message d'erreur lors de la lecture du fichier de configuration.
 *
 * @param lig	Numéro de ligne de l'erreur.
 * @param msg	Message d'erreur.
 */
static void log_err( int lig, char *msg )
{
	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	fprintf( stderr, "Ligne %d: %s.\n", lig, msg );
}
/*!
 * \brief Read  [www] configuration section.
 *
 *	Lecture de information de la section[www] du fichier de configuration.
 *
 *	port=entier			Définition du numéro de port d'écoute du service HTTP/HTTPS.
 *	root_user=chaine	Utilisateur authentifier sans mot de passe.
 *	ssl=[on|off]		Valide l'utilisation de HTTP (off) ou HTTPS (on).
 *	ssl_cert=chaine		Chemin du fichier contenant le certificat cryptant la clé pour HTTPS.
 *	sss_key=chaine		Chemin du fichier contenant la clé X509 pour HTTPS.
 *
 * @param nb		Nombre de section [www] à lire (inutilisé).
 * @param conf		Pointeur sur la configuration analysée.
 *
 * @return	0 en cas de succès.
 */
static int creer_www( int nb, CONF* conf )
{
	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	char **valeurs = NULL;
	int num = 0;
	int ret;

	ret = valeurs_configuration( conf, "www", num, "port", &valeurs );
	if( ret <= 0 ) {
		fprintf( stderr, "Pas de paramètre \"port\" dans la section \"[www]\" n° %d.\n", num );
		return 1;
	}
	if( Verbose > 1 ) {
		fprintf( stderr, "port=%s\n", valeurs[0] );
	}
	Server.port = strdup(valeurs[0]);

	ret = valeurs_configuration( conf, "www", num, "root_user", &valeurs );
	if( ret > 0 ) {
		if( RootUser ) free( RootUser );
		RootUser = strdup( valeurs[0 ] );
		if( Verbose > 1 ) {
			fprintf( stderr, "root=%s\n", valeurs[0] );
		}
	}

	Server.ssl = 0;
	ret = valeurs_configuration( conf, "www", num, "ssl", &valeurs );
	if( ret>0 && !strcmp( valeurs[0], "on" ) ) {
		Server.ssl = 1;
		/* Certificat */
		ret = valeurs_configuration( conf, "www", num, "ssl_cert", &valeurs );
		if( ret <= 0 ) {
			fprintf( stderr, "Pas de paramètre \"ssl_cert\" dans la section \"[www]\" n° %d.\n", num );
			return 1;
		}
		if( Verbose > 1 ) {
			fprintf( stderr, "ssl_cert=%s\n", valeurs[0] );
		}
		Server.ssl_cert = load_file(valeurs[0]);
		if( Server.ssl_cert == NULL ) {
			fprintf( stderr, "Cannot load certificat %s: %s\n", valeurs[0], strerror(errno) );
			return 1;
		}
		/* Key */
		ret = valeurs_configuration( conf, "www", num, "ssl_key", &valeurs );
		if( ret <= 0 ) {
			fprintf( stderr, "Pas de paramètre \"ssl_key\" dans la section \"[www]\" n° %d.\n", num );
			return 1;
		}
		if( Verbose > 1 ) {
			fprintf( stderr, "ssl_key=%s\n", valeurs[0] );
		}
		Server.ssl_key = load_file(valeurs[0]);
		if( Server.ssl_key == NULL ) {
			fprintf( stderr, "Cannot load key %s: %s\n", valeurs[0], strerror(errno) );
			return 1;
		}
	}

	return 0;
}
/*!
 * \brief Lecture du fichier de configuration.
 *
 * @param file	Chemin du fichier de configuration.
 *
 * @return 0 en cas de succès, sinon message d'erreur et errno positionné.
 */
int lire_config( char* const file )
{
	if( Verbose ) fprintf( stderr, "Enter %s( %s )\n", __func__, file );

	CONF* conf = parse_configuration( file, log_err );

	if( conf ) {
		int nb = nb_sections_configuration( conf, "www" );
		if( nb ) {
			if( creer_www( nb, conf ) ) {
				detruire_configuration( conf );
				errno = EINVAL;

				return 1;
			}
		}
	}

	detruire_configuration( conf );

	return conf != NULL ? 0 : 1;
}
/*!
 * \brief Send an HTTP error page.
 *
 *	Send an HTTP error with variable message.
 *
 * @param connection	handle to connection which is being processed.
 * @param code			HTTP error code.
 * @param message		HTML body message.
 *
 * @return MHD_YES if successful, MHD_NO if not.
 */
static int send_fmt_error( struct MHD_Connection* connection, int code, const char* message )
{
	GString* msg = g_string_new( NULL );

	g_string_printf( msg, HTTP_ERROR, message );
	struct MHD_Response* response = MHD_create_response_from_buffer( strlen(msg->str),
																		msg->str, MHD_RESPMEM_PERSISTENT );
	int ret = MHD_queue_response( connection, code, response );
	MHD_destroy_response( response );
	g_string_free( msg, TRUE );

	return ret;
}
/*!
 * \brief Send an HTTP error page.
 *
 *	Send an HTTP error with static message.
 *
 * @param connection	handle to connection which is being processed.
 * @param code			HTTP error code.
 * @param message		HTML message.
 *
 * @return MHD_YES if successful, MHD_NO if not.
 */
static int send_error( struct MHD_Connection* connection, int code, const char* message )
{
	struct MHD_Response* response = MHD_create_response_from_buffer( strlen(message),
																		(char*)message, MHD_RESPMEM_PERSISTENT );
	int ret = MHD_queue_response( connection, code, response );
	MHD_destroy_response( response );

	return ret;

}
/*!
 * \brief	URI logging.
 *
 *	Function called for URI logging (needed to collected parameters).
 *
 * @param cls	not used
 * @param uri	URI to be logged
 * @param con	handle to connection.
 *
 * @return Always NULL
 */
void * my_logger(void * cls, const char * uri, struct MHD_Connection *con )
{

	if( Verbose ) fprintf( stderr, "Enter %s( %s )\n", __func__, uri );

	if( pthread_mutex_lock( &StatsFileMtx ) != 0 ) return NULL;

	FILE* stat = fopen( StatFile, "a" );
	if( stat ) {
		const union MHD_ConnectionInfo *infos = MHD_get_connection_info( con, MHD_CONNECTION_INFO_CLIENT_ADDRESS );
		struct sockaddr *addr = infos->client_addr;

		char	from[256];
		int		ret = 0;

		from[0]='\0';
		if( (ret = getnameinfo( addr, sizeof(struct sockaddr), from, sizeof(from), NULL, 0, NI_NUMERICHOST )) != 0 ) {
			if( Verbose > 1 ) fprintf( stderr, "getnameinfo: %d, %s.\n", ret, gai_strerror(errno) );
		} else {
			char horo[32];
			time_t tim = time(NULL);
			strftime( horo, sizeof(horo), "%x %X", localtime(&tim) );
			fprintf( stat, "%s from: %s uri %s\n", horo, from, uri );
		}
		fclose( stat );
	}

	pthread_mutex_unlock( &StatsFileMtx );

	return NULL;
}
/*!
 * \brief	Ouverture d'un fichier authent.
 *
 *	Cette fonction effectue l'ouverture d'un fichier d'authentification pour un utilisateur donné 'user'.
 *	'ficAuth' pointe sur une zone fournie par l'appelant de taille suffisante pour contenir MAXPATHLEN caractères.
 *
 * @param user		Nom de l'utilisateur.
 * @param ficAuth	Zone contenant le chemin complet du fichier.
 *
 * @return	-1 (errno positionné) ou un descripteur sur le fichier ouvert.
 */
static int auth_open( char* user, char* ficAuth )
{
	char* rep = NULL;

	if( Verbose ) fprintf( stderr, "Enter %s( %s )\n", __func__, user );

	if( (rep = getenv("ACE_SUPRA_AUTH")) ) {
		snprintf( ficAuth, MAXPATHLEN, "%s/%s%s", rep, user, DEF_SUFFIX_FICAUTH );
	} else {
		rep = getenv( "ACEB" );
		if( rep == NULL ) {
			rep = getenv( "SAPHIRHOME" );
		}
		if( rep == NULL ) {
			rep = "/home/ace";
		}
		snprintf( ficAuth, MAXPATHLEN, "%s/conf/%s%s", rep, user, DEF_SUFFIX_FICAUTH );
	}

	if( Verbose > 1 ) fprintf( stderr, "%s:: open( %s )\n", __func__, ficAuth );

	int fd = open( ficAuth, O_RDONLY );
	if( fd < 0 ) {
		return -1;
	}

	return fd;
}
/*!
 * \brief	Iterator over key-value pairs.
 *
 *	 This iterator can be used to iterate over all of the cookies, headers, or
 * POST-data fields of a request, and also to iterate over the headers that have been added to a response.
 *
 * @param cls		custom value specified when iteration was triggered.
 * @param kind		kind of the header we are looking at.
 * @param key		key for the value, can be an empty string.
 * @param value		value corresponding value, can be NULL.
 *
 * @return	MHD_YES to continue iterating, MHD_NO to abort the iteration.
 */
enum MHD_Result arguments_it( void *cls, enum MHD_ValueKind kind, const char *key, const char *value )
{
	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	if( cls == NULL ) {
		sysjrnl( LOG_ERR, "arguments_it called without valid cls." );
		return MHD_NO;
	}
	// put key, value in KeyValue table pointed to by cls.
	struct KeyValue* tbl = cls;

	for( int i=0; i<tbl->nb; i++ ) {
		if( strcmp( tbl->data[i].key, key ) == 0 ) {
			tbl->data[i].val = (char*)value;
		}
	}
	tbl->data = realloc( tbl->data, (tbl->nb+1)*sizeof(tbl->data[0]) );
	tbl->data[tbl->nb].key = (char*)key;
	tbl->data[tbl->nb].val = (char*)value;
	tbl->nb++;

	if( Verbose > 1 ) {
		for( int i=0; i<tbl->nb; i++ ) {
			fprintf( stderr, "tbl->data[%d] = {\nkey=%s\nval=%s\n}\n", i, tbl->data[i].key, tbl->data[i].val );
		}
	}

	return MHD_YES;
}
/*!
 * \brief	Send configuration for an user.
 *
 *	Get access configuration for an user given as URI parameter.
 *
 * @param tbl	Key/value parameter table.
 *
 * @return MHD_YES if successful, MHD_NO if not.
 */
static int check_access( struct MHD_Connection* connection, struct KeyValue* tbl )
{
	char*	user = NULL;
	int		ret = -1;

	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	for( int i=0; i < tbl->nb; i++ ) {
		if( !strcmp( tbl->data[i].key, "user" ) ) {
			user = tbl->data[i].val;
			break;
		}
	}
	if( user == NULL ) {
		ret = send_error( connection, REQUEST_CODE, REQUEST_ERROR );
		return ret;
	}
	GString* json_out = g_string_new( NULL );
	char	ficAuth[MAXPATHLEN];
	int		fd = -1;

	fd = auth_open( user, ficAuth );
	if( fd < 0 ) {
		g_string_printf( json_out, "Internal error open ficAuth: %s.", strerror(errno) );
		ret = send_fmt_error( connection, 404, json_out->str );
	} else {
		if( do_access( fd, json_out, Verbose ) != 0 ) {
			ret = send_fmt_error( connection, 500, "Internal error" );
		} else {
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_success++;
				pthread_mutex_unlock( &Stats.mp );
			}
			struct MHD_Response* response = MHD_create_response_from_buffer( strlen(json_out->str),
					json_out->str, MHD_RESPMEM_PERSISTENT|MHD_RESPMEM_MUST_COPY );
			MHD_add_response_header( response, "Content-Type", "text/plain;charset=UTF-8" );

			ret = MHD_queue_response( connection, MHD_HTTP_OK, response );
			MHD_destroy_response( response );
		}
		close( fd );
	}
	g_string_free( json_out, TRUE );

	return ret;
}
/*!
 * \brief Authentifier l'utilisateur.
 *
 *	Analyse du dépot d'authentification centralisé pour déterminer si l'utilisateur cité
 * est bien authentifié.
 *
 * \param	user	Utilisateur à authentifier.
 * \param	pwd		Mot de passe de l'utilisateur.
 * \param	mach	Optionnel nom de l'atelier sur lequel l'utilisateur s'authentifie.
 */
static int do_authent( struct MHD_Connection* connection, struct KeyValue* tbl )
{
	char*	user = NULL;
	char*	pwd = NULL;
	char*	mach = NULL;
	char*	rep = NULL;
	char	ficAuth[MAXPATHLEN];
	int		ret = 1;

	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	for( int i=0; i < tbl->nb; i++ ) {
		if( !strcmp( tbl->data[i].key, "user" ) ) {
			user = tbl->data[i].val;
			continue;
		}
		if( !strcmp( tbl->data[i].key, "pwd" ) ) {
			pwd = tbl->data[i].val;
			continue;
		}
		if( !strcmp( tbl->data[i].key, "machine" ) ) {
			mach = tbl->data[i].val;
			continue;
		}
	}

	if( Verbose > 1 ) fprintf( stderr, "%s: user %s, pwd %s, mach %s\n", __func__, user?user:"NULL", pwd?pwd:"NULL", mach?mach:"NULL" );

	if( user == NULL ) {
		ret = send_error( connection, REQUEST_CODE, REQUEST_ERROR );
		return ret;
	}
	if( pwd == NULL ) {
		ret = send_error( connection, REQUEST_CODE, REQUEST_ERROR );
		return ret;
	}

	if( RootUser && !strcmp( user, RootUser ) ) {
		ret = send_error( connection, MHD_HTTP_OK, AUTHENT_OK );
		return ret;
	}

	int fd = auth_open( user, ficAuth );
	if( fd < 0 ) {
		if( Verbose > 1 ) fprintf( stderr, "Ouverture %s: %s.\n", ficAuth, strerror(errno) );
		ret = send_error( connection, AUTHENT_CODE, AUTHENT_ERROR );
		return ret;
	}

	struct stat bufStat;

	if( fstat( fd, &bufStat ) ) {
		close( fd );
		if( Verbose > 1 ) fprintf( stderr, "fstat %s: %s.\n", ficAuth, strerror(errno) );
		ret = send_error( connection, AUTHENT_CODE, AUTHENT_ERROR );
		return ret;
	}
	char* donnees = mmap( NULL, bufStat.st_size, PROT_READ, MAP_PRIVATE, fd, 0 );
	if( donnees == MAP_FAILED ) {
		close( fd );
		if( Verbose > 1 ) fprintf( stderr, "mmap %s: %s.\n", ficAuth, strerror(errno) );
		ret = send_error( connection, AUTHENT_CODE, AUTHENT_ERROR );
		return ret;
	}


	char* pt = NULL;

	if( (pt = strstr( donnees, "pwd = " )) ) {
		char* pt_pwd = pwd;
		pt += 6;
		while( *pt_pwd++ == *pt++ );
		if( *(pt_pwd-1) == '\0' && *(pt-1) == '\n' ) {
			if( Verbose > 1 ) fprintf( stderr, "pwd: %s %s: %s.\n", user, pwd, "OK" );
			ret = 0;
		}
	}

	if( !ret && mach ) {				// Si l'utilisateur est OK, on teste le comserv si demandé.
		size_t len = strlen( mach );
		pt = strchr( pt+6, '\n' );
		ret = 1;
		while( pt ) {
			if( (pt = strstr( pt, "ace = " )) ) {
				if( strncmp( mach, pt+6, len ) == 0 && *(pt+6+len) == '@' ) {
					if( Verbose > 1 ) fprintf( stderr, "mach: %s on %s: %s.\n", user, mach, "OK" );
					ret = 0;
					break;					// match found.
				}
			}
			if( pt ) {
				pt = strchr( pt+6, '\n' );	// next machine.
				continue;
			}
		}
	}

	munmap( donnees, bufStat.st_size );
	close(fd);

	if( Verbose > 1 ) fprintf( stderr, "%s %s %s: %d.\n", user, pwd, mach?mach:"NULL", ret );

	if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
		Stats.nb_success++;
		pthread_mutex_unlock( &Stats.mp );
	}
	if( ret == 0 ) {
		ret = send_error( connection, MHD_HTTP_OK, AUTHENT_OK );
	} else {
		ret = send_error( connection, AUTHENT_CODE, AUTHENT_ERROR );
	}

	return ret;
}
/*!
 * \brief Send a file
 *
 * @param connection	handle to connection which is being processed
 * @param file			file name relative to ROOT_DIR.
 *
 * @return HD_YES if successful, MHD_NO if not.
 */
static int send_file( struct MHD_Connection* connection, const char* file )
{
	GString*	html_index = g_string_new( NULL );
	int			ret;

	if( Verbose ) fprintf( stderr, "Enter %s( %s )\n", __func__, file );

	g_string_printf( html_index, "%s/%s", ROOT_DIR, file );
	int fd = -1;
	if( (fd = open( html_index->str, O_RDONLY )) < 0 ) {
		g_string_free( html_index, TRUE );

		struct MHD_Response* response = MHD_create_response_from_buffer( strlen(NOTFOUND_ERROR),
																			NOTFOUND_ERROR, MHD_RESPMEM_PERSISTENT );
		ret = MHD_queue_response( connection, NOTFOUND_CODE, response );
		MHD_destroy_response( response );

		return ret;
	}
	struct stat bufStat;
	if( fstat( fd, &bufStat ) != 0 ) {
		close( fd );
		g_string_free( html_index, TRUE );

		if( Verbose > 1 ) fprintf( stderr, "fstat %s: %s.\n", html_index->str, strerror(errno) );
		struct MHD_Response* response = MHD_create_response_from_buffer( strlen(NOTFOUND_ERROR),
																			NOTFOUND_ERROR, MHD_RESPMEM_PERSISTENT );
		ret = MHD_queue_response( connection, NOTFOUND_CODE, response );
		MHD_destroy_response( response );

		return ret;
	}

	if( Verbose > 1 ) fprintf( stderr, "%s: Call fstat( %s ): %ld\n", __func__, html_index->str, bufStat.st_size );
	char* donnees = mmap( NULL, bufStat.st_size, PROT_READ, MAP_PRIVATE, fd, 0 );
	if( donnees == MAP_FAILED ) {
		close( fd );
		if( Verbose > 1 ) fprintf( stderr, "mmap %s: %s.\n", html_index->str, strerror(errno) );
		ret = send_fmt_error( connection, 500, "Internal error when sending index.html.");
		return ret;
	}

	struct MHD_Response* response =
			MHD_create_response_from_buffer( bufStat.st_size, donnees, MHD_RESPMEM_PERSISTENT|MHD_RESPMEM_MUST_COPY );
	if( response == NULL ) {
		ret = send_fmt_error( connection, 500, "Internal error when sending index.html.");
	} else {
		if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
			Stats.nb_success++;
			pthread_mutex_unlock( &Stats.mp );
		}
		ret = MHD_queue_response( connection, MHD_HTTP_OK, response );
	}
	MHD_destroy_response( response );

	munmap( donnees,bufStat.st_size );
	close( fd );

	if( ret != MHD_YES ) {
		ret = send_fmt_error( connection, 500, "Internal error when sending index.html.");
	}

	return ret;
}
/*!
 * \brief	send statistics about program.
 *
 * @param connection handle to connection which is being processed
 *
 * @return MHD_YES
 */
static int send_stats( struct MHD_Connection* connection )
{
	GString* str = g_string_new( STAT_HEAD );

	if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
		const union MHD_ConnectionInfo* mhd_ci =
				MHD_get_connection_info( connection, MHD_CONNECTION_INFO_DAEMON );

		char horo[32];
		strftime( horo, sizeof(horo), "%x %X", localtime(&Stats.start) );

#ifdef MHD_DAEMON_INFO_CURRENT_CONNECTIONS
		const union MHD_DaemonInfo* mhd_info =
				MHD_get_daemon_info( mhd_ci->daemon, MHD_DAEMON_INFO_CURRENT_CONNECTIONS );
		g_string_append_printf( str, "<h1>HTTP Server</h1>Started since %s.<br># of connections: %u<br><br>",
				horo, mhd_info->num_connections );
#else
		g_string_append_printf( str, "<h1>HTTP Server</h1>Started since %s.<br><br>", horo );
#endif

		g_string_append_printf( str, "<h1>Services</h1>Nb requests:<i>%d</i><br>Success: <i>%d</i><br>"
				"Authentifier: <i>%d</i><br>Autoriser: <i>%d</i><br>Access: <i>%d</i><br>Other: <i>%d</i>"
				"</body></html>\n",
				Stats.nb_request,
				Stats.nb_success,
				Stats.nb_authent,
				Stats.nb_autor,
				Stats.nb_access,
				Stats.nb_upload );

		pthread_mutex_unlock( &Stats.mp );
	}
	if( Verbose > 2 ) {
		fprintf( stderr, "%s: %s\n", __func__, str->str );
	}
	struct MHD_Response* response =
			MHD_create_response_from_buffer( str->len, str->str, MHD_RESPMEM_PERSISTENT|MHD_RESPMEM_MUST_COPY );
	int ret = MHD_queue_response( connection, 200, response );
	MHD_destroy_response( response );

	g_string_free( str, TRUE );
	return MHD_YES;
}
/*!
 * \brief	Gestionnaire des événements HTTP.
 *
 *	Gestion de quelques événements du protocole HTTP (connexion, requêtes, ...)
 *
 *
 * @param cls				custom value selected at callback registration time
 * @param connection		handle to connection which is being processed
 * @param url				the URL requested by the client
 * @param method			the HTTP method used by the client (GET, PUT, DELETE, POST, etc.)
 * @param version			the HTTP version string (i.e. HTTP/1.1)
 * @param upload_data		the data being uploaded (excluding headers)
 * @param upload_data_size	set initially to the size of the upload_data provided
 * @param ptr				reference to a pointer, preserved by MHD for future calls for this request
 *
 * @return	MHD_YES if successfull, MHD_NO if the socket must be closed.
 */
static enum MHD_Result ev_handler( void *cls, struct MHD_Connection* connection,
		const char* url, const char* method, const char* version,
		const char* upload_data, size_t* upload_data_size,
		void **ptr )
{
	int ret = MHD_YES;
#ifdef HAVE_GETHRTIME
	hrtime_t	debut;
	hrtime_t	fin;

	debut = gethrtime();
#else
	struct timeval	debut;
	struct timeval	fin;

	gettimeofday( &debut, NULL );
#endif

	if( Verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	if( Verbose > 1 ) {
		fprintf( stderr, "Method: %s.\n", method );
		fprintf( stderr, "URL: %s.\n", url );
	}

	// We have received an HTTP request.
	// Send HTTP reply to the client which shows full original request.
	if( !strcmp( method, MHD_HTTP_METHOD_GET ) || !strcmp( method, MHD_HTTP_METHOD_HEAD )) {
		// Find which page
		if( !strcmp( url, "/" ) || !strcmp( url, "/index.html" ) ) {
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_upload++;
				pthread_mutex_unlock( &Stats.mp );
			}
			ret = send_file( connection, INDEX );
		} else if( !strcmp( url, "/favicon.ico" ) ) {
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_upload++;
				pthread_mutex_unlock( &Stats.mp );
			}
			ret = send_file( connection, "favicon.ico" );
		} else if( !strcmp( url, "/theme.css" ) ) {
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_upload++;
				pthread_mutex_unlock( &Stats.mp );
			}
			ret = send_file( connection, "theme.css" );
		} else if( !strcmp( url, "/stats" ) ) {
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_upload++;
				pthread_mutex_unlock( &Stats.mp );
			}
			ret = send_stats( connection );
		} else if( !strcmp( url, "/authentifier" ) ) {	// Only user, passwd
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_authent++;
				pthread_mutex_unlock( &Stats.mp );
			}
			struct KeyValue key_value_tbl = { 0, NULL };
			(void)MHD_get_connection_values( connection, MHD_GET_ARGUMENT_KIND, arguments_it, &key_value_tbl );
			ret = do_authent( connection, &key_value_tbl );
			free( key_value_tbl.data );
			key_value_tbl.nb = 0;
		} else if( !strcmp( url, "/autorisation" ) ) {	// User, password and machine.
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_autor++;
				pthread_mutex_unlock( &Stats.mp );
			}
			struct KeyValue key_value_tbl = { 0, NULL };
			(void)MHD_get_connection_values( connection, MHD_GET_ARGUMENT_KIND, arguments_it, &key_value_tbl );
			ret = do_authent( connection, &key_value_tbl );
			free( key_value_tbl.data );
			key_value_tbl.nb = 0;
		} else if( !strcmp( url, "/access" ) ) {		// JSON formatted user record.
			if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
				Stats.nb_access++;
				pthread_mutex_unlock( &Stats.mp );
			}
			struct KeyValue key_value_tbl = { 0, NULL };
			(void)MHD_get_connection_values( connection, MHD_GET_ARGUMENT_KIND, arguments_it, &key_value_tbl );
			ret = check_access( connection, &key_value_tbl );
			free( key_value_tbl.data );
			key_value_tbl.nb = 0;
		} else {
			struct MHD_Response* response = MHD_create_response_from_buffer( strlen(NOTFOUND_ERROR), NOTFOUND_ERROR, MHD_RESPMEM_PERSISTENT );
			ret = MHD_queue_response( connection, NOTFOUND_CODE, response );
			MHD_destroy_response( response );
		}
	} else {
		// Method not allowed
		struct MHD_Response* response = MHD_create_response_from_buffer( strlen(METHOD_ERROR), METHOD_ERROR, MHD_RESPMEM_PERSISTENT );
		ret = MHD_queue_response( connection, METHOD_CODE, response );
		MHD_destroy_response( response );
	}
#ifdef HAVE_GETHRTIME
	fin= gethrtime();
	long long int duree = fin - debut;
#else
	gettimeofday( &fin, NULL );
	long long int duree = ((fin.tv_sec - debut.tv_sec) * 1000000 + (fin.tv_usec - debut.tv_usec)) * 1000;
#endif
	if( pthread_mutex_lock( &Stats.mp ) == 0 ) {
		Stats.nb_request++;
		Stats.duration += duree;
		pthread_mutex_unlock( &Stats.mp );
	}

	return ret;
}
/*!
 * \brief	Lancement du thread serveur HTTP.
 *
 * @param arg	Informations de configuration du serveur HTTP.
 */
static void* start_http_server( void* arg )
{
	ldmls_www_t* 			 server = (ldmls_www_t*)arg;

	char* rep = getenv( "ACEB" );
	if( rep == NULL ) {
		rep = getenv( "SAPHIRHOME" );
	}
	if( rep == NULL ) {
		rep = "/home/ace";
	}
	if( chdir( rep ) != 0 ) {
		sysjrnl( LOG_ERR, "HTTP daemon died, chdir(%s): %m.", rep );
		return NULL;
	}

	Stats.start = time(NULL);

	struct MHD_Daemon*	mhd = NULL;

	// start daemon
	if( Server.ssl ) {
		// start daemon with SSL on
		mhd = MHD_start_daemon(
				MHD_USE_THREAD_PER_CONNECTION|
				MHD_USE_INTERNAL_POLLING_THREAD|
				MHD_USE_SSL|
				MHD_USE_DEBUG,									/* flags */
				(unsigned short)atoi( server->port ),			/* port */
				NULL,											/* MHD_AcceptPolicyCallback apc */
				NULL,											/* void *apc_cls */
				ev_handler,										/* MHD_AccessHandlerCallback dh */
				NULL,											/* void *dh_cls */
				MHD_OPTION_CONNECTION_TIMEOUT,	10,
				MHD_OPTION_URI_LOG_CALLBACK,	my_logger, NULL,
				MHD_OPTION_HTTPS_MEM_KEY,		Server.ssl_key,
				MHD_OPTION_HTTPS_MEM_CERT,		Server.ssl_cert,
				MHD_OPTION_END );
	} else {
		mhd = MHD_start_daemon(
				MHD_USE_THREAD_PER_CONNECTION|
				MHD_USE_INTERNAL_POLLING_THREAD|
				MHD_USE_DEBUG,									/* flags */
				(unsigned short)atoi( server->port ),			/* port */
				NULL,											/* MHD_AcceptPolicyCallback apc */
				NULL,											/* void *apc_cls */
				ev_handler,										/* MHD_AccessHandlerCallback dh */
				NULL,											/* void *dh_cls */
				MHD_OPTION_CONNECTION_TIMEOUT,	10,
				MHD_OPTION_URI_LOG_CALLBACK,	my_logger, NULL,
				MHD_OPTION_END );

	}
	if( mhd == NULL ) {
		sysjrnl( LOG_ERR, "HTTP daemon cannot start. Invalid MHD_run call." );
		return NULL;
	}
	MHD_run( mhd );
	while (1) {
		sleep( 10000 );
	}

	sysjrnl( LOG_NOTICE, "HTTP daemon stops." );
	MHD_stop_daemon( mhd );

	return NULL;
}
/*
 *	Usage message.
 */
static void usage( char *n )
{
	fprintf( stderr, "Usage: %s -V -v -c config\n", n );
	fprintf( stderr, "avec:\n-V mode verbeux.\n-v indique la version.\n" );
	fprintf( stderr, "-c fichier de configuration.\n" );
}
/*
 *	Programme principal.
 */
int main( int argc, char *argv[] )
{
	char		*file	= "saceauth.conf";
	int			 option_index = 0;
	int			 c = 0;

	while( (c = getopt_long( argc, argv, "vVc:", Long_Options,  &option_index )) != -1 ) {
		switch( c ) {
			case 'v':
				fprintf( stderr, "Version: %s\n", VERSION );
				return EXIT_SUCCESS;
			case 'V':
				Verbose++;
				break;
			case 'c':
				file = optarg;
				break;
			default:
				fprintf( stderr, "L'option %c est inconnue.\n", c );
				usage( argv[0] );
				return EXIT_FAILURE;
		}
	}

	if( !Verbose ) {
		int ret = -1;
		if( (ret = fork()) == -1 ) {
			return -1;
		}
		if( ret ) {
			sysjrnl( LOG_NOTICE, "Demarrage de %s: pid = %d\n", argv[0], ret );
			return 0;
		}
		/*
		 *  Dupliquer stdout et stderr.
		 */
		char log[1024];
		snprintf( log, sizeof(log), "%s/jrnl/saceauth.out", getwdir() );
		if( (ret = open( log, O_WRONLY|O_CREAT,  0660 )) < 0 ) {
			sysjrnl( LOG_ERR, "%s::open::jrnl/mservid.out: %m.", argv[0] );
			return 1;
		}
		if( dup2( ret, 2 ) < 0 ) perror( "dup(stderr)" );
		if( dup2( ret, 1 ) < 0 ) perror( "dup(stdout)" );
		/*
		 * Clore les canaux encore ouverts
		 */
		close( 0 );
		for( int i = 3; i < ret; i++ ) close( i );
		close( ret );
		/*
		 * Detacher le processus du terminal.
		 */
		if( setsid() < 0 ) {
			sysjrnl( LOG_ERR, "%s::setsid(): %m.", argv[0] );
			return 1;
		}
	}

	if( lire_config( file ) ) {
		sysjrnl( LOG_ERR, "Configuration parsing error: %m." );
		return EXIT_FAILURE;
	}

	snprintf( StatFile, sizeof(StatFile), "%s/jrnl/saceauth_access", getwdir() );

	if( pthread_mutex_init( &Stats.mp, NULL ) < 0 ) {
		sysjrnl( LOG_NOTICE, "Initialisation mutex stats: %m." );
	}
	if( pthread_mutex_init( &StatsFileMtx, NULL ) < 0 ) {
		sysjrnl( LOG_NOTICE, "Initialisation mutex log: %m." );
	}

	if( start_http_server( &Server ) ) {
		sysjrnl( LOG_ERR, "Erreur au lancement du serveur WEB. Serveur désactivé.\n" );
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
