%{
/*!
 * \file auth2json_l.l
 *
 * \brief   Conversion fichier authent.supra en JSON.
 *
 *	Analyse lexical des fichier de type .supra. Découpage en token.
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jean-paul.ecochard@certira.cnafmail.fr>
 * \date   jeu. mai 17 10:58:27 CET 2018
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

 
#include <stdio.h>
#include <gmodule.h>

#include "saceauth-auth2json_y.h"
%}
LE	[A-Za-z]
CH	[0-9]
A	[Aa]
B	[Bb]
C	[Cc]
D	[Dd]
E	[Ee]
F	[Ff]
G	[Gg]
H	[Hh]
I	[Ii]
J	[Jj]
K	[Kk]
L	[Ll]
M	[Mm]
N	[Nn]
O	[Oo]
P	[Pp]
Q	[Qq]
R	[Rr]
S	[Ss]
T	[Tt]
U	[Uu]
V	[Vv]
W	[Ww]
X	[Xx]
Y	[Yy]
Z	[Zz]
/*
 * Pas de yywrap
 */
%option noyywrap
%%
[ \t\n]				;
^#.*$				;
<<EOF>>				return FFIC;
{U}{S}{E}{R}			return USER;
{C}{O}{D}{E}			return CODE;
{P}{W}{D}			return PWD;
{P}{R}{O}{F}{I}{L}		return PROFIL;
{D}{R}{O}{I}{T}			return DROIT;
{A}{C}{E}			return ACE;
=				return EQ;
@				return SEP;
\[				return TABOPEN;
\]				return TABCLOSE;
,				return TABSEP;
\{				return '{';
\}				return '}';
({LE}|{CH})({LE}|{CH}|_|-)+	return ID;
%%
#ifndef TEST
int do_access( int fd, GString* json_out, int verbose ) {
	if( verbose ) fprintf( stderr, "Enter %s()\n", __func__ );

	struct stat buf;

	if( fstat( fd, &buf ) < 0 ) {
		return -1;
	}
	char* supra = mmap( NULL, buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0 );
	if( supra == MAP_FAILED ) {
		return -1;
	}
	YY_BUFFER_STATE buffer = yy_scan_bytes( supra, buf.st_size );
	yyparse( json_out );
	if( verbose > 2 ) fprintf( stderr, "%s:: json_out = %s\n", __func__, json_out->str );
	if( verbose > 1 ) fprintf( stderr, "%s:: size = %d\n", __func__, json_out->len );
	yy_flush_buffer( buffer );
	yy_delete_buffer( buffer );
	munmap( supra, buf.st_size );

	return 0;
}
#endif
